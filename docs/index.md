## A propos de moi

![](images/avatar-photo.jpg)

Bonjour !

Je m'appelle **Manon VANAVERMAETE** !

Après une année de design industriel suivi d’un bachelier en Architecture d’intérieur à ENSAV-La Cambre (Bruxelles) mon envie d’élargir mes connaissances n’a cessé d’évoluer. En effet, durant l’année 2019-2020 j’ai réalisé une année de passerelle à l’université de Liège afin d’avoir accès à un master en architecture. Le parcours que j’ai accompli jusqu’à présent a permis de confirmer cette passion pour la relation entre l’humain et l’espace qui m’anime.

C’était donc la suite logique de mes aspirations que de pouvoir intérejoindre le master d’architecture de La Cambre Horta. J’ai décidé de rejoindre cette faculté parce que c’est, à mon sens, une école qui pousse les étudiants à la création en étant ouvert d’esprit et en regardant vers l’avenir. 

Mon parcours est le reflet de ma personnalité, en effet en tant qu'architecte/designer, je m'interresse à toutes les échelles des projets. Allant du plan urbanistique, à l'architecture générale, l'architecture d'intérieur jusqu'au design des plus petites pièces de mobilier, mes préoccupations sont partout !

N'hésitez pas à visiter ce site pour voir une partie de mon travail !
