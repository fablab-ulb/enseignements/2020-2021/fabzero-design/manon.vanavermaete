# 3. MODULE 3 : IMPRESSION 3D

Lors de la troisième formation, nous avons appris à employer les imprimantes 3D. Les imprimantes de notre fablab sont des "Original Prusa i3 MK3S". 
Je vais donc vous expliquer ce que j'ai appris à faire durant ce module, mais aussi, vous montrer mes échecs (parce qu'il y en a), c'est important de voir l'évolution de mon travail pour comprendre comment je suis arrivée au bout de mon projet.

1.	L’imprimante

Peu de marques peuvent se targuer de jouir d’une réputation aussi impeccable que celle de Prusa Research, ni d’être entourées d’une communauté d’enthousiastes aussi fervente.
Basé en République tchèque, le fabriquant Josef Prusa s’efforce constamment d’améliorer chaque aspect de son écosystème Open Source, du matériel jusqu’au logiciel. Une détermination que ses fans apprécient particulièrement. Les derniers upgrades apportés à l’imprimante 3D Prusa i3 MK3 s’inspirent d’ailleurs de leurs expérriences et commentaires.

## Avantages :
•	Qualité d’impression exceptionnelle
•	Solidité
•	Communauté enthousiaste et coopérative
•	Autocalibrage
•	Détection des chocs
•	Mise en pause et reprise d’impression
•	Ponts et surplombs de qualité
•	Prise en main facile du logiciel de découpe Slic3r PE
•	Réactivité et compétence du service client Prusa
## Inconvénients :
•	Peu fiable pour les longs travaux d’impression
•	Structures de support denses avec les paramètres par défaut
•	Pas de mise à jour majeure comparée à l’ancienne i3 MK3

![](../images/fab12.png)

2.	Les paramètres

Sur le logiciel de Slicer Prusa, il faut, en tout premier lieu, régler toute une série de paramètres qui correspondent au modèle d’imprimante que l’on a à notre disposition. Vous pouvez voir ces différents paramètres dans les captures d’écran ci-dessous. 

![](../images/fab13.png)

![](../images/fab14.png)

![](../images/fab15.png)

![](../images/fab16.png)

3.	Imprimer

D’abord il faut prendre la carte SD qui se trouve dans l’imprimante qui est à notre disposition. Ensuite la glisser dans son ordinateur (j’avais, heureusement, un ordinateur muni d’un lecteur de carte SD). 

Préalablement, j’avais exporté le G-Code de mon fichier sur Prusa (auxquelles, pour rappel, je n’avais ajouté aucun support). 

J’ai donc importé mon G-Code sur la carte SD de l’imprimante. 

Une fois la carte réinsérée dans l’imprimante, il faut sélectionner (à l’aide de la molette) le fichier qu’on a réalisé et qui apparait sur la petite interface. 

![](../images/fab17.png)

Ça y est ! L’impression est lancée, on observe assez rapidement le dessin interne de l’objet en Giroïde. Il vaut mieux rester quelques minutes à proximité de la machine de façon à s’assurer que l’objet s’imprime convenablement et qu’il n’y a aucun problème au lancement. 

4.	Résultat

Malheureusement il y a eu quelques problèmes lors de l'impression de la dernière couche. Effectivement la machine semble avoir buté sur une bavure et avoir perdu sa trajectoire. Affaire à suivre…

![](../images/fab18.png)



