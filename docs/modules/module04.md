# 4. MODULE 4 : DECOUPE LASER

1. Présentation de la Lasersaur

La lasersaur est une découpeuse laser Open Source dont les seuls composants non standards sont : 
•	des panneaux en acryliques découpées au laser
•	des pièces d'assemblage mécaniques découpées au laser dans de l'aluminium
Tout la partie mécanique est composée de profilé d'aluminium avec des pièces d'assemblage et d'autres pièces standards. 
L'électronique est composée d'une grosse plaque PCB avec un Atmega 328 (le même micro-contrôleur que l'arduino UNO), un ordinateur beaglebone et des drivers moteur GECKO. Deux transformateurs (5V et 24V) permettent d'alimenter la carte.  

![](../images/fab23.png)

2. les matériaux 

- le bois contreplaqué de 3 ou 4 mm 
- le plexiglass 
- le papier 
- le carton 

Il faut savoir que certains matériaux qui produise des fumées extrêmement nocives sont fortement déconseillés. Parmi ceux-ci on retrouve notamment le MDF (à cause de la colle synthétique qu’il contient), les matières fibreuses, le métal, le PVC, le cuivre, le Téflon, le vinyl, le simili-cuir et l’époxy.

Le polystyrène, le PE, PET et le PP sont également des matériaux déconseillés pour l’utilisation de la lasersaur car les découpes sont peu précises (j’en ai d’ailleurs fait l’expérience avec du polypropylène). 

3. Le fichier 

Avant de pouvoir entreprendre la gravure ou découpe désirée, il faut d’abord passer par un logiciel de dessin vectoriel. Moi j’ai employé VectorWorks (c’est le logiciel que j’utilise habituellement pour mes dessins d’architecture et de design). 

J’ai donc d’abord dessiné mon objet en plan avec les plis et encoches nécessaires. Ensuite, il faut mettre en couleurs. 
En effet, pour que le laser différencie les endroits où il doit découper de ceux où il doit graver il faut colorer les lignes différemment (couleur au choix). Moi J’ai mis en rouge les découpes et en noir les gravures (qui permettent, en réalité, de réaliser un pliage facilement). 

Ensuite il faut exporter le fichier en SVG et l’importer dans le logiciel « Inskape ». Celui-ci permet de s’assurer que les lignes sont indépendantes les unes des autres et que l’ensemble du dessin est bien dégroupé. A la fin de ce control il faut à nouveau exporter le fichier en SVG.

Enfin, la dernière étape est d’importer le document SVG dans l’application de la lasersaur (sur l’ordinateur du fablab). Normalement le fichier s’ouvre sans trop de problème. Ce ne fût pas mon cas… Malheureusement mon fichier apparaissait complètement vierge et l’application annonçait le message « aucun objet ». Après quelques autres tentatives d’import non concluantes, j’ai compris que le problème était dû à mes lignes qui était beaucoup trop fines et donc illisible par la lasersaur. J’ai donc repassé mon document sur Inskape et j’ai épaissis les lignes de mon projet. J’ai également redéfini les couleurs de découpes et de gravure depuis Inskape de façon à m’assurer que le document soit parfaitement prêt. 

Après avoir, à nouveau, importé mon fichier sur l’application de la lasersaur mon document était enfin lisible et accepté. 

![](../images/fab21.png)

4.	Utilisation de la Lasersaur

La première étape, c’est de placer la feuille de PP dans la machines et de régler la distance entre le lqser et lq feuille. Celq ce fait grâce à un gabarit. Ensuite, il faut retourner sur l’ordinateur, et voir si le périmètre est convenablement détecté. Pour cela il faut cliquer sur la petite maison en bas de l’écran puis sur les deux flèches sur le haut de l’écran. Là, la machine se met en route et l’on peut voir le périmètre qu’elle détecte. Si celui-ci est bon, on peut lancer l’opération.

La machine fait beaucoup de bruit, pas de panique, et elle est rapidement enfumée (il ne faut pas oublier d’ouvrir les vannes d’aspiration et souffleries). 

![](../images/fab22.png)

5. Ma création

Moi j’ai réalisé une lampe qui reprend le model de ma radio T3 sous forme de pliage en PP. Je l’ai réalisé à plusieurs échelles de façon à créer un petit ensemble à disposer. Je trouvais qu’il était intéressant de reprendre le model tel qu’il a été dessiné par Dieter Rams mais en changeant sa fonction. En effet la lumière se diffuse non-seulement grâce à la translucidité du matériaux employé (PP contrairement à Rams qui avait utilisé du PLA) et surtout grâce aux petits trous (dont la fonction initiale est de diffuser le son, mais ici, la lumière avec un jeux d’ombre). 

![](../images/fab19.png)

![](../images/fab20.png)
