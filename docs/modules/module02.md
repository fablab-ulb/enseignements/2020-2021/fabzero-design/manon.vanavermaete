# 2. MODULE 2 : DESSIN CAO

Avant de pouvoir imprimer en 3D, nous avons appris à employer un logiciel de dessin tridimensionnel durant la première formation accélérée avec Thibault Baes. Le nom de ce logiciel c’est « Fusion 360 ». C'est un des logiciels les plus employés par les designers d’objets et les designers industriels pour modéliser un objet en 3D et l'imprimer ensuite. 
L’exercice qui a suivi la formation, et qui nous a permis de s’entrainer à modéliser avec Fusion 360, c’est tenter de redessiner l’objet que nous avions choisi (lors de la visite du musée Adam en début d’année).  L’objet que j’avais choisi c’était la radio T3 que le grand design de la marque « Braun » Dieter Rams a dessiné dans les années 1960. 
La première étape que j’ai entreprise avant d’attaquer la modélisation c’est me familiariser avec cette radio portative. J’ai tenté de comprendre (sur base des photos que j’avais pris lors de la visite du musée et des informations que j’ai trouvé sur internet) les dimensions générales de l’objet, ses proportions et particularités (de façon à ce que l’objet soit imprimé à la bonne échelle). 


## MATERIALITE et DIMENSIONS

Le model T3 est l’ancêtre des lecteurs mp3, c’est l’une des premières radios portables. Ce petit boitier blanc de forme rectangulaire mesure plus au moins 120mm sur 65mm sur 25mm (dimension que j’ai approximées en regardant l’objet derrière sa vitrine au musée). La partie supérieure est perforée de petits trous pour la diffusion du son, ils sont disposés de façon à créer un carré et laissant plus au moins 0,5mm entre la fin de la zone perforée et les extrémités de l’objet. Il y a un total de 100 perforations (10 sur 10). La partie du dessous est, quant à elle, munie d’une petite molette circulaire permettant de régler l’intensité du volume sonore de l’appareil/choisir la station radio. Enfin, Dieter Rams a décidé de congédier les angles de cette petite radio T3. 
C’est une pièce de design industriel iconique datant de 1958. Honnête et compréhensible en un clin d’œil donc il n’a pas été trop compliqué pour moi de l’appréhender et de dessiner le plan.
L’ensemble de l’objet est réalisé en un seule et unique matériau : L'acrylonitrile butadiène styrène ou ABS. C’est un polymère thermoplastique présentant une bonne tenue aux chocs, relativement rigide, léger et pouvant être moulé. Il appartient à la famille des polymères styréniques. 

![](../images/fab1.png)

C'est surtout le matériau privilégié pour les appareils électroménagers. Les carters d'aspirateurs et les corps de cafetières sont souvent en ABS. Dans ce cas, il s'agit le plus souvent de pièces injectées. 
Sous forme de fil, il est utilisé dans les imprimantes 3D. 
Le fabricant de jouets danois Lego l'utilise pour fabriquer ses briques de construction. C'est une des particularités qui leur confèrent d'ailleurs leur qualité. L'ABS, en raison de ses propriétés anti-chocs, est encore de nos jours le plastique utilisé pour faire des carrosseries entières (voitures sans permis).
Exposé aux rayons ultraviolets, certains plastiques ABS contenant des agents ignifuges bromés ont tendance à jaunir (c’est d’ailleurs un détail que j’ai remarqué sur la pièce du musée.

![](../images/fab2.png)

## MODELISATION DE L'OBJET

Avant de visualiser l’objet en 3D, j’ai du passer par toutes sortes d’étapes que je vais tenter de vous résumer en vous présentant un résumé de mes essais et erreurs. La bonne nouvelle, c’est que mon objet n’est pas très compliqué, en effet il ne s’agit que d’une simple boite rectangulaires perforée (c’est d’ailleurs ce qui fait la force de son design !). Mon objet est donc relativement facile à modéliser en 3D (ce qui me rassure puisque c’est une première pour moi sur Fusion 360). En plus La forme sera très facilement imprimable en 3D sans devoir y ajouter de support ou autres subterfuges pour éviter qu’il ne tombe ou se déforme sur le plateau de l’imprimante. 

1.	Dessiner le plan

La première étape pour le dessin du plan de base de mon parallélépipède c’est de commencer une esquisse. C’est à partir de ce moment-là que je peux tracer mon rectangle sur le plan que j’ai choisi. Je sélectionne donc l’outil rectangle, je le fais partir de l’origine de mon plan de travail (sur Fusion 360) et j’encode les dimensions générales. Ensuite il ne faut pas oublier de « terminer l’esquisse ».

2.	Création de la boite

Ma forme tridimensionnelle est un parallélépipède rectangle. Ayant déjà tracé mon plan général, il ne me reste qu’à l’extruder.

![](../images/fab3.png)

3.	Mise en forme du congé

L’objet que Dieter Rams a dessiné est légèrement arrondi au niveau des angles. J’ai donc reproduit cet effet/ce détail sur mon objet modélisé. Pour cela, rien de plus simple, il faut juste sélectionner l’objet et ensuite, l’outil « congé » permet de créer de légers ou forts arrondis aux angles de notre forme en fonction du paramètre que l’on indique. 

![](../images/fab4.png)

4.	Les perforations
Cette étape a été plus difficile à comprendre que les autres parce que, pour moi, elle aura été moins intuitive. J’ai choisi volontairement de ne pas véritablement trouer mon objet (qui était destiné a être imprimer en 3D comme une simple maquette et pas un objet électronique fonctionnel). La stratégie que j’ai mise en place, pour simuler les perforations, c’est d’extruder des cercles vers la partie interne de la boite. 
J’ai d’abord sélectionné la face de l’objet (que je voulais « perforer »). Ensuite, avec l’outil « perçage » j’ai fait un premier essais peu concluant car le trou était beaucoup trop grand. 

![](../images/fab5.png)

J’ai vite compris que ce que je devais faire c’est créer une nouvelle esquisse sur la face supérieure de l’objet cette fois-ci. Dans cette esquisse j’ai dessiné les petits cercles du haut-parleur à l’aide de l’outil « cercle ».

![](../images/fab6.png)
![](../images/fab7.png)
![](../images/fab8.png)

Ca fonctionne !!

J’annule donc ce que je viens de faire et complète mon esquisse avec le bon nombre de cercles en les dupliquant par groupe. Puis je réitère l’opération vue précédemment (pour le perçage). 

![](../images/fab9.png)

Je profite de cette étape pour dessiner la mollette de l’appareil et je créé une légère extrusion vers le bas pour lui donner une sensation de relief (je veux que l’objet ressemble parfaitement au vrai lors de la prise en mains et du touché). 

![](../images/fab10.png)

1.	Et voilà, c’est terminé ! 
Mon objet est près pour l’étape suivante (le passage sur l’application Prusa et l’impression 3D). 

![](../images/fab11.png)


