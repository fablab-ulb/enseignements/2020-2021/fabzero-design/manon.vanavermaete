# 1. Module 1

Cette semaine je me suis familiarisé avec gitlab et j'ai mis à jour mon site. Durant le premier labo, nous avons creer notre propre environement sur base d'un template fourni. Il est structuré avec un fichier index (Homepage de mon site), un dossier modules et un dossier images.
Utiliser Git permet en fait d'avoir une copie locale de ce site et de mettre à jour automatiquement le serveur (GitLab) avec les changements que je fais en local.
Les étapes sont :
- Installer le logiciel GIT sur mon ordinateur
- Configurer GIT
- Créer un dossier local pour acceuillir les fichiers de mon site
- Copier les fichiers dans ce dossier.

## Installer GIT

Pour installer GIT, je suis aller dans Google et rechercher "How to install GIT on Mac"

[Installing GIT - GIT](https://git-scm.com/book/en/v2/Getting-Started-Installing-Git/)

J ai trouvé un site qui propose différentes methodes. J'ai décidé d'utiliser le "binary install" et donc cliquer sur ce lien.
![](../images/Download_GIT.png)

Une fois copier, il faut regler la securité du Mac pour autoriser l'installation d'un logiciel qui ne vient pas du AppsStore.
Pour cela, il faut aller dans les "preferences system" puis "Security" et autoriser l'installation.
![](../images/Allow_install.png)

Ensuite l'installation se passe sans problème.

Pour verifier si l'installation s'est bien effectuée, on ouvre le logiciel "Terminal" et on lance la commande

_git --version_

le terminal affiche alors la version de GIT.

## Configurer GIT

Il faut définir le nom utilisateur qui doit être le même que celui utilisé dans GitLab. Pour moi

_git config --global user.name "manon.vanavermaete"_

on peut verifier si c'est OK

_git config --global user.name_

Terminal affiche alors 

_manon.vanavermaete_

## creer un dossier en local

pour acceuillir tous les fichiers et dossiers existant sur GitLab, il faut créer un dossier.
J'ai créer mon dossier _GitRepository_ dans le dossier _documents_ de mon mac.
Il faut positioner Terminal dans ce dossier par la commande

_cd documents/_GitRepository_


## Copier les fichiers dans ce dossier

On va creer un clone du dossier manon.vanavermaete qui contient toutes les données depuis GilLab. Pour cela, il faut connaitre le lien (URL) du dossier dans GitLab. Ce lien peut se copier via le bouton Clone disponible sur GitLab.

![](../images/Copy_URL.png)

enfin on lance le clonage dans Terminal par la commance

_git clone Pomme Paste_

La copie commence

![](../images/Commands_in_Terminal.png)

Il suffit de verifier si la copie s'est bien passé en regardant directement via le _Finder_ du mac.

![](../images/Check_copy_of_files.png)


C est reussi !!



